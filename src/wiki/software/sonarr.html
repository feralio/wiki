<h1>Sonarr</h1>
<p>This article will show you how to install Sonarr. <a href="https://sonarr.tv/">Sonarr</a> is a smart PVR for newsgroup and BitTorrent users.</p>

<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a>
                <ul>
                    <li><a href="#pre-config">Configuring before starting Sonarr</a></li>
                </ul>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping and restarting</a>
                <ul>
                    <li><a href="#auto-restart">Automatically restarting Sonarr if it is not running</a></li>
                </ul>
            </li>
            <li>Configuring
                <ul>
                    <li><a href="#password">Setting a password in Sonarr</a></li>
                    <li><a href="#configure-clients">Configuring clients with Sonarr</a>
                        <ul>
                            <li><a href="#rtorrent">rTorrent</a></li>
                            <li><a href="#deluge">Deluge</a></li>
                            <li><a href="#transmission">Transmission</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="installation">Installation</h2>
<p>Install or update Sonarr by logging in via SSH, then copying and pasting the following:</p>
<pre><kbd>wget -qO ~/sonarr.tar.gz 'https://services.sonarr.tv/v1/download/main/latest?version=3&os=linux'
rm -rf ~/Sonarr
tar xf ~/sonarr.tar.gz
rm ~/sonarr.tar.gz
mkdir -p ~/.config/Sonarr
mkdir ~/tmp
# Start v3 to create DB
pkill -f -9 Sonarr/Sonarr
TMPDIR=~/tmp nohup -- mono --debug Sonarr/Sonarr.exe -nobrowser 2>&1 >/dev/null &
sleep 10

# Install v4
pkill -f -9 Sonarr/Sonarr
rm -rf ~/Sonarr
wget -qO ~/sonarr.tar.gz 'https://services.sonarr.tv/v1/download/main/latest?version=4&os=linux&arch=x64'
tar xf ~/sonarr.tar.gz
rm ~/sonarr.tar.gz</kbd></pre>

<h3 id="pre-config">Configuring before starting Sonarr</h3>
<p>You then need to tweak the config slightly for the first time use. You can skip this step if you are updating an existing install. You can do this by copying and pasting the following into SSH:</p>
<pre><kbd>sed -i 's|&lt;Port&gt;8989&lt;/Port&gt;|&lt;Port&gt;'$(shuf -i 10001-32001 -n 1)'&lt;/Port&gt;|g' ~/.config/Sonarr/config.xml
sed -i 's|&lt;UrlBase&gt;&lt;/UrlBase&gt;|&lt;UrlBase&gt;/'"$(whoami)"'/sonarr&lt;/UrlBase&gt;|g' ~/.config/Sonarr/config.xml
echo "http://$(hostname -f):$(sed -rn 's|(.*)&lt;Port&gt;(.*)&lt;/Port&gt;|\2|p' ~/.config/Sonarr/config.xml)/$(whoami)/sonarr/"</kbd></pre>

<p>After Sonarr has been started you <em>must</em> <a href="#password">set a password</a>.</p>

<h2 id="start-stop-restart">Starting, stopping and restarting</h2>
<p>This section covers the Sonarr process - starting it, stopping it and restarting it. It also covers checking if the process is running, in case that becomes necessary.</p>

<p>You can start Sonarr and print its URL with the following:</p>
<pre><kbd>screen -dmS sonarr /bin/bash -c 'export TMPDIR=~/tmp; Sonarr/Sonarr -nobrowser' && echo "http://$(hostname -f):$(sed -rn 's|(.*)&lt;Port&gt;(.*)&lt;/Port&gt;|\2|p' ~/.config/Sonarr/config.xml)/$(whoami)/sonarr/"</kbd></pre>

<p>If you need to check that it's running, you can run the command below. If the process is running a list of relevant process ID numbers will be listed. If nothing is listed, the process is not running.</p>
<p><kbd>pgrep -f Sonarr</kbd></p>
<p>The Sonarr process can by stopped by executing:</p>
<p><kbd>pkill -f Sonarr</kbd></p>
<p>If Sonarr is unresponsive then it can be "forced" to stop by executing:</p>
<p><kbd>pkill -f -9 Sonarr</kbd></p>
<p>If Sonarr needs to be restarted, copy and paste these commands:</p>
<pre><kbd>pkill -f Sonarr
screen -dmS sonarr /bin/bash -c 'export TMPDIR=~/tmp; Sonarr/Sonarr -nobrowser'
echo "http://$(hostname -f):$(sed -rn 's|(.*)&lt;Port&gt;(.*)&lt;/Port&gt;|\2|p' ~/.config/Sonarr/config.xml)/$(whoami)/sonarr/"</kbd></pre>

<h3 id="auto-restart">Automatically restarting Sonarr if it is not running</h3>
<p>Cron jobs can be used to check if Sonarr is running and start it up if it is not. There is a separate page on <a href="../slots/cron">configuring cron jobs</a>.</p>

<h2>Configuring</h2>
<p>This section covers setting a password in Sonarr and connecting the Sonarr to different clients.</p>

<h3 id="password">Setting a password in Sonarr</h3>
<p>Within the Sonarr UI, click the <samp>Settings</samp> icon (the three gears at the top) and then click the <samp>General</samp> tab. Under <samp>Security</samp>, click the dropdown menu next to <samp>Authentication</samp> and select <samp>Forms (Login page)</samp>. Two fields will appear and you'll be able to set a username and password for the web UI. Whilst it's not mandatory you do this, it is a sensible thing to do. You'll need to restart the program for the changes to take effect.</p>

<h3 id="configure-clients">Configuring clients with Sonarr</h3>
<p>In Sonarr's user interface click the <samp>Settings</samp> icon (the three gears at the top) and then click <samp>Download Client</samp>. Next, click the <kbd>+</kbd> button. You can choose the torrent client you want to configure from the menu.</p>

<h4 id="rtorrent">rTorrent</h4>
<p>First of all you need to create the rTorrent RPC and this is done by <a href="nginx">switching from apache to nginx</a>.</p>
<dl>
    <dt>Name</dt>
    <dd>Your chosen name for the settings - you can pick what you like</dd>
    <dt>Enable</dt>
    <dd><kbd>Yes</kbd></dd>    
    <dt>Host</dt>
    <dd><kbd><var>server</var>.feralhosting.com</kbd></dd>
    <dt>Port</dt>
    <dd><kbd>443</kbd></dd>
    <dt>Url Path</dt>
    <dd><kbd>/<var>username</var>/rtorrent/rpc</kbd></dd>
    <dt>Use SSL</dt>
    <dd><kbd>Yes</kbd></dd>
    <dt>Username</dt>
    <dd><kbd>rutorrent</kbd> (the word 'rutorrent', <em>not</em> your ruTorrent username)</dd>
    <dt>Password</dt>
    <dd>Your ruTorrent access password</dd>
</dl>
<p>In the above settings, <var>server</var> is replaced by the name of your server (e.g. zeus) and <var>username</var> is your username on the server.</p>

<h4 id="deluge">Deluge</h4>
<p>First make sure <samp>Advanced Settings</samp> have been set to <samp>Shown</samp>, in order for the necessary options to appear.</p>
<dl>
    <dt>Name</dt>
    <dd>Your chosen name for the settings - you can pick what you like</dd>   
    <dt>Enable</dt>
    <dd><kbd>Yes</kbd></dd>    
    <dt>Host</dt>
    <dd><kbd><var>server</var>.feralhosting.com</kbd></dd>
    <dt>Port</dt>
    <dd><kbd>443</kbd></dd>
    <dt>Url Base</dt>
    <dd><kbd>/<var>username</var>/deluge</kbd></dd>
    <dt>Password</dt>
    <dd>Your Deluge web UI password</dd>
    <dt>Use SSL</dt>
    <dd><kbd>Yes</kbd></dd>
</dl>
<p>In the above settings, <var>server</var> is replaced by the name of your server (e.g. zeus) and <var>username</var> is your username on the server.</p>

<h4 id="transmission">Transmission</h4>
<p>First make sure <samp>Advanced Settings</samp> have been set to <samp>Shown</samp>, in order for the necessary options to appear.</p>
<dl>
    <dt>Name</dt>
    <dd>Your chosen name for the settings - you can pick what you like</dd>   
    <dt>Enable</dt>
    <dd><kbd>Yes</kbd></dd>    
    <dt>Host</dt>
    <dd><kbd><var>server</var>.feralhosting.com</kbd></dd>
    <dt>Port</dt>
    <dd><kbd>443</kbd></dd>
    <dt>Url Base</dt>
    <dd><kbd>/<var>username</var>/transmission/</kbd></dd>
    <dt>Username</dt>
    <dd>Your Transmission username</dd>
    <dt>Password</dt>
    <dd>Your Transmission password</dd>
    <dt>Use SSL</dt>
    <dd><kbd>Yes</kbd></dd>
</dl>
<p>In the above settings, <var>server</var> is replaced by the name of your server (e.g. zeus) and <var>username</var> is your username on the server.</p>

<h2 id="uninstallation">Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and its config files - back up important data first!</aside>

<pre><code>pkill -9 -f Sonarr; rm -rf ~/.config/Sonarr/ ~/Sonarr</code></pre>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="https://sonarr.tv/">Sonarr's homepage</a></li>
    <li><a href="https://sonarr.tv/#support">Sonarr support</a></li>
</ul>