#!/usr/bin/env bash
#set -eux -o pipefail

# Stop any existing processes
rm ~/.config/feral/user-container.d/wiki-qbittorrent 2>/dev/null || true
pkill -9 -f qbittorrent-nox || true

# Binary
mkdir -p ~/bin
wget -qO ~/bin/qbittorrent-nox https://github.com/userdocs/qbittorrent-nox-static/releases/download/release-5.0.1_v2.0.10/x86_64-qbittorrent-nox
chmod +x ~/bin/qbittorrent-nox

# Auto-restart
mkdir -p ~/.config/feral/user-container.d
echo -e '#!/usr/bin/env bash
set -eux -o pipefail
[[ "$(pgrep -f '''qbittorrent-nox''')" ]] || ~/bin/qbittorrent-nox' >~/.config/feral/user-container.d/wiki-qbittorrent
chmod +x ~/.config/feral/user-container.d/wiki-qbittorrent

# Configure
salt="$(dd if=/dev/urandom count=4 bs=4 2>/dev/null | base64)"
pass="$(tr -dc A-Za-z0-9 </dev/urandom | head -c 15 ; echo '')"
hmac="$(php -r "echo base64_encode(hash_pbkdf2(\"sha512\", \"$pass\", base64_decode(\"$salt\"), 100000, 0, true));")"
mkdir -p ~/.config/qBittorrent
echo "[LegalNotice]
Accepted=true
[BitTorrent]
Session\AsyncIOThreadsCount=1
Session\HashingThreadsCount=1
[Preferences]
Bittorrent\MaxConnecs=40
Bittorrent\MaxConnecsPerTorrent=-1
Bittorrent\MaxUploads=-1
Bittorrent\MaxUploadsPerTorrent=-1
Queueing\QueueingEnabled=false
WebUI\Username=$(whoami)
WebUI\Password_PBKDF2=\"@ByteArray($salt:$hmac)\"
WebUI\Port=9001
WebUI\MaxAuthenticationFailCount=60
Downloads\SavePath=$HOME/private/qbittorrent/data" >~/.config/qBittorrent/qBittorrent.conf

# Reverse proxy
bits=$(( $(id -u) << 1 ))
upper=$(( $bits >> 8 ))
lower=$(( $bits - ($upper << 8) + 1 ))
ip="10.0.$upper.$lower"
echo "location /qbittorrent/ {
    proxy_pass http://$ip:9001/;
    proxy_http_version 1.1;
    proxy_set_header   Host               \$http_x_host;
    proxy_set_header   X-Forwarded-Host   \$http_x_host;
    proxy_set_header   X-Forwarded-For    \$proxy_add_x_forwarded_for;
}" >~/.nginx/conf.d/000-default-server.d/wiki-qbittorrent.conf
/usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf 2>/dev/null

# Report
echo qBittorrent has been successfully installed. It will be running in five minutes.
echo 
echo URL: https://$(hostname -f)/$(whoami)/qbittorrent/
echo Username: $(whoami)
echo Password: $pass
