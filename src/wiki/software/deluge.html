<h1>Deluge</h1>
<p>This article will take you through the installation, usage, and customisation of the torrent client Deluge. For the most part no expertise is required to use the guide. Some areas will require being able to <a href="../slots/ssh">connect to your slot via SSH</a> and you will be instructed to connect via SSH where this is the case.</p>
<p>Deluge works in two parts - a daemon and a means of controlling it. The daemon is the "engine" and is responsible for the core torrent-related functionality. There are three means of controlling the daemon - through a web UI, through a thin client remotely and through a console.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#install_deluge">Installation</a></li>
            <li>Accessing Deluge
                <ul>
                    <li><a href="#daemon-details">Obtaining the Deluge daemon's details</a></li>
                    <li><a href="#deluge-webui">WebUI</a></li>
                    <li><a href="#deluge-thin-client">Thin client</a></li>
                    <li><a href="#deluge-console">Console</a></li>
                </ul>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping and restarting</a></li>
            <li>Usage
                <ul>
                    <li><a href="#basic-considerations">Basic considerations</a></li>
                    <li><a href="#adding-torrents">Adding torrents</a></li>
                    <li><a href="removing-torrents">Removing torrents</a></li>
                </ul>
            </li>
            <li><a href="#deluge-plugins">Plugins</a></li>
            <li><a href="#upgrading-downgrading">Upgrading and downgrading</a>
                <ul>
                    <li><a href="#custom-version-install">Installing a custom version</a></li>
                    <li><a href="#custom-version-configure">Configuring the custom version before use</a></li>
                    <li><a href="#custom-version-start">Starting the custom instance</a></li>
                </ul>
            </li>
            <li><a href="#multiple-instances">Running multiple instances of Deluge</a>
                <ul>
                    <li><a href="#multiple-instances-setup">Creating directories and copying existing files</a></li>
                    <li><a href="#multiple-instances-configure">Configuring the new instance</a></li>
                    <li><a href="#multiple-instances-access">Accessing the new instance</a></li>
                    <li><a href="#multiple-instances-start">Starting the extra instance</a></li>
                    <li><a href="#multiple-instances-auto-restart">Automatically restarting custom instances</a></li>
                </ul>
            </li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="install_deluge">Installation</h2>
<p>It is very simple to install or reinstall Deluge as the Feral web manager supports its installation. Please follow the separate guide to <a href="../getting-started/web-manager#install">installing software from the web manager</a>, selecting Deluge from the list.</p>
<p>You will need to download and install the thin client separately from <a href="http://dev.deluge-torrent.org/wiki/Download">Deluge's own download page</a>. Simply select the operating system from the list and follow the installation guide for software on your operating system. You should select Python 2.7 where possible.</p>

<h2>Accessing Deluge</h2>

<h3 id="daemon-details">Obtaining the Deluge daemon's details</h3>
<p>However you choose to control the Deluge daemon it is likely that you'll need its details at some point. To get them, log in via SSH then copy and paste the following one-line command:</p>

<pre><kbd>printf "10.0.0.1\n$(hostname -f)\n$(whoami)\n$(sed -rn 's/(.*)"daemon_port": (.*),/\2/p' ~/.config/deluge/core.conf)\n$(sed -rn "s/$(whoami):(.*):(.*)/\1/p" ~/.config/deluge/auth)\n"</kbd></pre>

<p>The output of that command will be 5 separate lines. The first will always be <samp>10.0.0.1</samp>. Whilst in reality you'll get your actual details returned for the other four, in order to explain what each one means these have been reduced to 4 separate variables:</p>
<ul>
    <li><var>hostname</var></li>
    <li><var>user</var></li>
    <li><var>port</var></li>
    <li><var>password</var></li>
</ul>

<p>Those variables are defined as follows:</p>
<dl>
    <dt><dfn>host</dfn></dt>
    <dd>The hostname of the server you are on. Used for connecting the daemon from somewhere other than your slot, such as the thin client.</dd>
    <dt><dfn>user</dfn></dt>
    <dd>Your username for the Deluge daemon.</dd>
    <dt><dfn>port</dfn></dt>
    <dd>The port that the Deluge daemon is running on. This will be different for each user.</dd>
    <dt><dfn>password</dfn></dt>
    <dd>The Deluge daemon password. It is not the same thing as the web UI password.</dd>
</dl>
<p>The following sections rely on you being able to obtain this information and they each use the variables described above.</p>

<h3 id="deluge-webui">Web UI</h3>
<p>To access the web UI simply visit the link as provided on your slot's software page. This will be in the format <kbd>https://<var>server</var>.feralhosting.com/<var>user</var>/deluge/</kbd>, where <var>server</var> is the name of your server and <var>user</var> is your username.</p>
<p>If for some reason the daemon has been removed from your web UI connection manager, you can add it by clicking on <samp>Connection Manager</samp> and adding the requested information. For host, you can use <samp>10.0.0.1</samp>. For the other fields use the corresponding information from the section Obtaining the Deluge daemon's details.</p>

<h3 id="deluge-thin-client">Thin client</h3>
<p>To configure the Deluge daemon in the thin client simply supply the information for <var>host</var> in the field <samp>hostname</samp>, together with <var>user</var>, <var>port</var> and <var>password</var> as in the section Obtaining the Deluge daemon's details.</p>

<p>The thin client 2.x is not backwards compatible with 1.x versions. Feral runs a 1.x version to avoid breaking existing set ups.</p>

<h3 id="deluge-console">Console</h3>
<p>This article's coverage of the console only extends to running it on your slot via SSH. It should in principle be possible to do entirely remotely but probably does not add much value above the existing options. This is particularly the case for Windows since <samp>deluge-console.exe</samp> cannot be run in interactive mode (i.e. the user must pass commands individually and then manually check to see the current status of torrents).</p>
<p>To access the console via SSH, log in to your slot and run <kbd>deluge-console</kbd>. You will initially get an error stating <samp>Failed to connect to 127.0.0.1:58846 with reason: Connection refused</samp>. This is because the software tries Deluge's default daemon values, whereas for obvious reasons Feral needs to change these for each user.</p>
<p>To connect to your daemon, replace the following variables with the information you got in the section Obtaining the Deluge daemon's details:</p>
<p><kbd>connect 10.0.0.1:<var>port</var> <var>user</var> <var>password</var></kbd></p>

<h2 id="start-stop-restart">Starting, stopping and restarting</h2>
<p>As described in the introduction Deluge runs in two parts, the daemon and some means of controlling it. This section covers starting, stopping and restarting the Deluge daemon (process name <samp>deluged</samp>). It is not possible to restart the web UI since it doesn't actually run on your accessible slot. You must therefore let the system start the web UI. 
</p>
<p>Every five minutes the system will scan the processes running and if either the Deluge daemon or web UI is not running it will attempt to start them up. If you need to start, stop or restart the Deluge daemon you can do so by logging in via SSH and executing the following commands.</p>
<dl>
    <dt>start</dt>
    <dd><kbd>deluged<kbd></dd>
    <dt>check running</dt>
    <dd><kbd>pgrep -fu "$(whoami)" "deluged"</kbd></dd>
    <dt>stop</dt>
    <dd><kbd>pkill -fu "$(whoami)" 'deluged'</kbd></dd>
    <dt>restart</dt>
    <dd><kbd>pkill -fu "$(whoami)" 'deluged' &amp;&amp; sleep 3 &amp;&amp; deluged</kbd></dd>
    <dt>kill (force stop)</dt>
    <dd><kbd>pkill -9 -fu "$(whoami)" 'deluged'</kbd></dd>
</dl>
<p>If you get any errors running these commands please check the troubleshooting section.</p>
<p>The <samp>check running</samp> command will return a process number if the Deluge daemon is running. If it doesn't return anything, the Deluge daemon is not running.</p>

<h2>Usage</h2>
<p>This section provides information on the most basic use of Deluge. It generally tackles things in the order web UI, thin client, console.</p>

<h3 id="basic-considerations">Basic considerations</h3>
<p>Deluge prioritises downloading over uploading, particularly if the disk is under heavy load. The priority for Deluge is to increase availability for the swarm. For this reason and for improved performance in general, it does follows that you see do <em>not</em> best speeds by setting the number of possible connections to a high or unlimited number. You would be more likely hurting than helping yourself by setting a high number since disk activity is required in the creation and maintenance of connections.</p>
<p>Whilst Deluge is a solid choice for lower numbers of torrents it can become unstable or sluggish at higher numbers. It is not possible to pinpoint an exact number, but at around 600 is when you may start to see effects. As the torrent numbers increase, so does the chance for instability and sluggishness. If you need to run a higher number of torrents you should perhaps consider your options - running multiple instances of Deluge (see the specific section below) or running Deluge together with rTorrent.</p>
<p><strong>Please note</strong> that since it is the daemon running on your Feral space which does the actual torrent work, you do <em>not</em> need to keep your computer running if you wish to use the thin client. You would simply use the thin client when you wanted to add or remove torrents; when you shut your machine down the daemon will continue downloading or uploading.</p>
<p>If you decide to use the console, you can quit it by typing <kbd>quit</kbd> and pressing <kbd>enter</kbd>.</p>

<h3 id="adding-torrents">Adding torrents</h3>
<p>If you're using the web UI, click on the <samp>Add</samp> button on the top of the UI to bring up the <samp>Add Torrents</samp> dialog. You can add multiple torrents if you so wish and can add either via a <samp>.torrent</samp> file or directly via a URL.</p>
<p>In the thin client the button is a large <samp>+</samp> sign with tooltip text which reads <samp>Add torrent</samp>. You can also press <samp>ctrl</samp> + <samp>O</samp> to bring up the dialog.</p>
<p>Things are done in the console using commands. To add a torrent, use <kbd>add</kbd>. You can get help for any commands (including <kbd>add</kbd>) using the option <kbd>-h</kbd>:</p>
<pre><samp>add -h
Usage: add [-p &lt;save-location&gt;] <var>&lt;torrent-file/infohash/url&gt;</var> [&lt;torrent-file/infohash/url&gt; ...]

Options:
  -p PATH, --path=PATH  save path for torrent
  -h, --help            show this help message and exit

Add a torrent</samp></pre>
<p>The options should be fairly simple - those surrounded by <samp>[</samp> and <samp>]</samp> are not mandatory - use them only if you want to. So, if you want a save location different to your default one, use the <kbd>-p</kbd> option, otherwise you can leave it. Replace <var>&lt;torrent-file/infohash/url&gt;</var> with a torrent location on your slot, an infohash or a URL.</p>

<h3 id="removing-torrents">Removing torrents</h3>
<p>In both the web UI and the thin client, select the torrent or torrents you wish to remove from the list of torrents. Then, <kbd>right click</kbd> and select <samp>Remove Torrent</samp>. You'll then be presented to three options:</p>
<dl>
    <dt>Cancel</dt>
    <dd>Take no action - no torrents are removed</dd>
    <dt>Remove with Data</dt>
    <dd>The torrent is removed completely - from Deluge and your slot in general</dd>
    <dt>Remove Torrent</dt>
    <dd>The torrent is removed from Deluge, but its data remains on your slot.</dd>
</dl>
<p>If you use the console, use the <kbd>rm</kbd> command and its associated options:</p>
<pre><samp>>>> rm -h
Usage: rm <var>&lt;torrent-id&gt;</var>

Options:
  --remove_data  remove the torrent's data
  -h, --help     show this help message and exit

Remove a torrent</samp></pre>
<p>The <var>torrent-id</var> should be replaced with either the name or the hash of the torrent. You can type <kbd>rm</kbd> and then press the <kbd>tab</kbd> key twice to list the torrent names and their hashes, or first execute <kbd>info</kbd> to display the torrents and their information (including ID hashes). If you also want to remove the data from the disk itself, remember to specify <kbd>--remove_data</kbd>.</p>

<h2 id="deluge-plugins">Plugins</h2>
<p>A separate article covers <a href="deluge/plugins">installing plugins for Deluge</a>.</p>

<h2 id="upgrading-downgrading">Upgrading and downgrading</h2>
<p>Feral does not support any other version of Deluge than the one installable via the web manager. If a new version comes out which you want to switch to, or you need an earlier version for some reason, you'll need to install it manually.</p>

<p>Please note the following, when installing custom versions of Deluge:</p>
<ul>
    <li>You should first install the version from the web manager if you want to use the web UI.</li>
    <li>You'll need to manually manage the starting and restarting of the Deluge daemon.</li>
    <li>Custom versions will only work with Deluge 1.x</li>
</ul>

<h3 id="custom-version-install">Installing a custom version</h3>
<p>To install the custom version you need to grab the source from the <a href="http://download.deluge-torrent.org/source/?C=M;O=D">Deluge source download listing</a>. Replace <var>deluge_source</var> in the first command below with the <samp>.tar.gz</samp> file representing the version you want. Run the following commands whilst connected to your slot via SSH:</p>
<pre><samp>wget -qO ~/deluge-tmp.tar.gz <var>deluge_source</var>
mkdir ~/deluge-tmp &amp;&amp; tar xf ~/deluge-tmp.tar.gz  --strip-components=1 -C ~/deluge-tmp &amp;&amp; cd ~/deluge-tmp/
python setup.py install --user
cd &amp;&amp; rm -rf ~/deluge-tmp*</samp></pre>

<h3 id="custom-version-configure">Configuring the custom version before use</h3>
<pre><kbd>sed -i 's|"daemon_port": 58846|"daemon_port": '$(shuf -i 10001-32001 -n 1)'|g' ~/.config/deluge/core.conf
sed -i 's|"download_location": "$(pwd)/Downloads"|"download_location": "$(pwd)/private/deluge/data"|g' ~/.config/deluge/core.conf
sed -i 's|"autoadd_location": "$(pwd)/Downloads"|"autoadd_location": "$(pwd)/private/deluge/watch"|g' ~/.config/deluge/core.conf
sed -i 's|"torrentfiles_location": "$(pwd)/Downloads"|"torrentfiles_location": "$(pwd)/private/deluge/watch"|g' ~/.config/deluge/core.conf
sed -i 's|"max_connections_global": 200|"max_connections_global": 40|g' ~/.config/deluge/core.conf
echo "$(whoami):$(&lt; /dev/urandom tr -dc '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' | head -c20; echo;):10" &gt; ~/.config/deluge/auth
printf "10.0.0.1\n$(hostname -f)\n$(whoami)\n$(sed -rn 's/(.*)"daemon_port": (.*),/\2/p' ~/.config/deluge/core.conf)\n$(sed -rn "s/$(whoami):(.*):(.*)/\1/p" ~/.config/deluge/auth)\n"</kbd></pre>
<p>The final command will print the access details which you can use as per the section Accessing Deluge.</p>

<h3 id="custom-version-start">Starting the custom instance</h3>
<p>The custom instance of can be started by running <kbd>~/.local/bin/deluged</kbd>. It can be stopped as per the main section Starting, stopping and restarting</p>

<h2 id="multiple-instances">Running multiple instances of Deluge</h2>
<p>Feral can only support the instance of Deluge as installed via the web manager. However, it should still be possible to install and run further instances. The lack of support means that only general help (in addition to that given by this article) can be given if you open a ticket. </p>
<p>To execute the commands given in this section you will need to log on via SSH.</p>
<p>You are not limited to one extra instance - simply follow the steps replacing <kbd>deluge2</kbd> with <kbd>deluge3</kbd> and so on (or indeed any other name you wish).</p>

<h3 id="multiple-instances-setup">Creating directories and copying existing files</h3>
<p>The initial steps are simply to create the necessary directories in ~/private for the new instance of Deluge as well as copying the existing instance's config files to make things a little easier on us. Copy and paste the following:</p>
<pre><kbd>mkdir -p ~/private/deluge2/{completed,data,torrents,watch}
cp -r ~/.config/deluge ~/.config/deluge2</kbd></pre>

<h3 id="multiple-instances-configure">Configuring the new instance</h3>
<p>Some of the files copied as part of the last section are unnecessary and should be removed. We also need to alter the configuration a little bit. Copy and paste the following.</p>
<pre><kbd>rm -f ~/.config/deluge2/deluged.pid
rm -f ~/.config/deluge2/state/*.torrent
sed -i 's|/private/deluge/|/private/deluge2/|g' ~/.config/deluge2/core.conf
sed -i 's|/.config/deluge/|/.config/deluge2/|g' ~/.config/deluge2/core.conf
sed -i 's|"daemon_port":.*,|"daemon_port": '$(shuf -i 10001-32001 -n 1)',|g' ~/.config/deluge2/core.conf</kbd></pre>

<h3 id="multiple-instances-access">Accessing the new instance</h3>
<p>Firstly, print the details for the new instance by copying and pasting the following:</p>
<pre><kbd>printf "10.0.0.1\n$(hostname -f)\n$(whoami)\n$(sed -rn 's/(.*)"daemon_port": (.*),/\2/p' ~/.config/deluge2/core.conf)\n$(sed -rn "s/$(whoami):(.*):(.*)/\1/p" ~/.config/deluge2/auth)\n"</kbd></pre>
<p>Then, start up the new instance by copying and pasting this command:</p>
<p><kbd>deluged -c ~/.config/deluge2/</kbd></p>
<p>You can then add the details to your access method of choice (web UI, thin client or console) as per the section Accessing Deluge.</p>

<h3 id="multiple-instances-start">Starting the extra instance</h3>
<p>The extra instance of can be started by running <kbd>deluged -c ~/.config/deluge2/</kbd>. It can be stopped as per the main section Starting, stopping and restarting, swapping <kbd>"deluged"</kbd> with <kbd>"deluge2"</kbd> in the commands.</p>

<h3 id="multiple-instances-auto-restart">Automatically restarting custom instances</h3>
<p>Whilst the main instance of Deluge installable via the web manager will be started automatically, cron jobs can be used to check if custom daemons are running and start them up if they are not. There is a separate page on <a href="../slots/cron">configuring cron jobs</a>.</p>

<h2 id="Troubleshooting">Troubleshooting</h2>
<dl>
    <dt>Restarting the Deluge daemon gives the error <samp>Couldn't listen on any:<var>port</var>: Errno 98 Address already in use.</samp></dt>
    <dd><p>In the error above, <var>port</var> is replaced by a number (likely beginning 52 or 53). The error means another process, perhaps another user's, is running on that process. Please open a ticket quoting the error message and staff will investigate.</p></dd>
    <dt>In the web UI I get the error <samp>"The connection to the webserver has been lost!"</samp></dt>
    <dd>
        <p>This error can have a number of causes. It might just be that the Deluge daemon needs restarting. This seems particularly to be the case if the daemon has been running for a long time. Please see the section above, Starting, stopping and restarting.</p>
        <p>It might also be that you're running too many torrents for Deluge to handle and stability is suffering as a result. You can either create another instance as per the guide 'Running multiple instances of Deluge' or <a href="../slots/torrent-client-migration">migrate to another torrent client</a>. Please note that it's perfectly possible to run different clients side-by-side without issue.</p>
    </dd>
</dl>

<h2 id="uninstallation">Uninstalling</h2>
<aside class="alert note">The commands below will completely remove the software, downloaded data and its config files - back up important data first!</aside>
<pre><kbd>pkill -9 -fu "$(whoami)" 'deluged'
pkill -9 -fu "$(whoami)" 'deluge-web'
rm -rf ~/.config/deluge ~/private/deluge</kbd></pre>
<p>These commands act only on your slot, so the software page will not change to reflect the fact that Deluge has been removed. The URL and password will remain despite the fact that Deluge is gone.</p>

<h2 id="external-links">External links</h2>
<ul>
    <li>
        <a href="http://deluge-torrent.org/">Deluge homepage</a>
        <ul>
            <li><a href="http://dev.deluge-torrent.org/wiki/Plugins">Deluge plugins</a></li>
            <li><a href="http://dev.deluge-torrent.org/wiki/UserGuide">Official support</a></li>
            <li><a href="http://forum.deluge-torrent.org/">Community</a></li>
        </ul>
    </li>
</ul>