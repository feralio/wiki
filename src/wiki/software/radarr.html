<h1>Radarr</h1>
<p><a href="https://radarr.video/">Radarr</a> is a movie collection manager for Usenet and BitTorrent users.</p>

<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a>
                <ul>
                    <li><a href="#pre-config">Configuring before starting Radarr</a></li>
                </ul>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping and restarting</a>
                <ul>
                    <li><a href="#auto-restart">Automatically restarting Radarr if it is not running</a></li>
                </ul>
            </li>
            <li>Configuring
              <ul>
                <li><a href="#password">Setting a password in Radarr</a></li>
                <li>Configuring clients with Radarr
                  <ul>
                    <li><a href="#rtorrent">rTorrent</a></li>
                    <li><a href="#deluge">Deluge</a></li>
                    <li><a href="#transmission">Transmission</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="installation">Installation</h2>
<p>Install Radarr by logging in via SSH, then copying and pasting the following:</p>
<pre><kbd>wget -qO ~/radarr.tar.gz 'https://radarr.servarr.com/v1/update/master/updatefile?os=linux&amp;runtime=netcore&amp;arch=x64'
tar xf ~/radarr.tar.gz
rm ~/radarr.tar.gz
mkdir -p ~/.config/Radarr/tmp</kbd></pre>

<h3 id="pre-config">Configuring before starting Radarr</h3>
<p>A config.xml file needs to be created and tweaked before we can properly run Radarr. To create it, use the following command:</p>
<p><kbd>nano ~/.config/Radarr/config.xml</kbd></p>
<p>Then, copy and paste the following:</p>
<pre><samp>&lt;Config&gt;
  &lt;Port&gt;7878&lt;/Port&gt;
  &lt;UrlBase&gt;&lt;/UrlBase&gt;
  &lt;BindAddress&gt;*&lt;/BindAddress&gt;
  &lt;SslPort&gt;9898&lt;/SslPort&gt;
  &lt;EnableSsl&gt;False&lt;/EnableSsl&gt;
  &lt;ApiKey&gt;&lt;/ApiKey&gt;
  &lt;AuthenticationMethod&gt;None&lt;/AuthenticationMethod&gt;
  &lt;LogLevel&gt;Info&lt;/LogLevel&gt;
  &lt;Branch&gt;develop&lt;/Branch&gt;
  &lt;LaunchBrowser&gt;False&lt;/LaunchBrowser&gt;
  &lt;SslCertHash&gt;&lt;/SslCertHash&gt;
  &lt;UpdateMechanism&gt;BuiltIn&lt;/UpdateMechanism&gt;
  &lt;AnalyticsEnabled&gt;False&lt;/AnalyticsEnabled&gt;
&lt;/Config&gt;</samp></pre>
<p>Once you're done hold <kbd>ctrl</kbd> + <kbd>x</kbd> to save. Press <kbd>y</kbd> to confirm.</p>

<p>You then need to tweak the config slightly. You can do this by copying and pasting the following:</p>
<pre><kbd>sed -i 's|&lt;Port&gt;7878&lt;/Port&gt;|&lt;Port&gt;'$(shuf -i 10001-32001 -n 1)'&lt;/Port&gt;|g' ~/.config/Radarr/config.xml</kbd></pre>

<h2 id="start-stop-restart">Starting, stopping and restarting</h2>
<p>This section covers the Radarr process - starting it, stopping it and restarting it. It also covers checking if the process is running, in case that becomes necessary.</p>
<p>You can start Radarr and print its URL with the following:</p>
<pre><kbd>screen -dmS Radarr /bin/bash -c 'export TMPDIR=~/.config/Radarr/tmp; ~/Radarr/Radarr -nobrowser' &amp;&amp; echo "http://$(hostname -f):$(sed -rn 's|(.*)&lt;Port&gt;(.*)&lt;/Port&gt;|\2|p' ~/.config/Radarr/config.xml)/"</kbd></pre>
<p>If nothing happens or there is an error it could be that the port that was picked in the configuration step in use. Simply run these commands again to try with a different number:</p>
<pre><kbd>sed -i 's|&lt;Port&gt;.*&lt;/Port&gt;|&lt;Port&gt;'$(shuf -i 10001-32001 -n 1)'&lt;/Port&gt;|g' ~/.config/Radarr/config.xml
screen -dmS Radarr /bin/bash -c 'export TMPDIR=~/.config/Radarr/tmp; ~/Radarr/Radarr -nobrowser' &amp;&amp; echo "http://$(hostname -f):$(sed -rn 's|(.*)&lt;Port&gt;(.*)&lt;/Port&gt;|\2|p' ~/.config/Radarr/config.xml)/"</kbd></pre>

<p>If you need to check that it's running, you can run the command below. If the process is running a list of relevant process ID numbers will be listed. If nothing is listed, the process is not running.</p>
<p><kbd>pgrep -laf Radarr</kbd></p>
<p>The Radarr process is stopped by executing:</p>
<p><kbd>pkill -f Radarr</kbd></p>
<p>If Radarr needs to be restarted, copy and paste these commands:</p>
<pre><kbd>pkill -f Radarr
screen -dmS Radarr /bin/bash -c 'export TMPDIR=~/.config/Radarr/tmp; ~/Radarr/Radarr -nobrowser'
echo "http://$(hostname -f):$(sed -rn 's|(.*)&lt;Port&gt;(.*)&lt;/Port&gt;|\2|p' ~/.config/Radarr/config.xml)/"</kbd></pre>

<p>Please note that if you want to stop the process for Radarr, it's best to check it afterwards to make sure it has stopped. If the process has crashed and will not stop when requested, you can kill it with:</p>
<p><kbd>pkill -9 -fu "$(whoami)" 'Radarr'</kbd></p>

<h3 id="auto-restart">Automatically restarting Radarr if it is not running</h3>
<p>Cron jobs can be used to check if Radarr is running and start it up if it is not. There is a separate page on <a href="../slots/cron">configuring cron jobs</a>.</p>

<h2>Configuring</h2>
<p>This section covers setting a password in Radarr and connecting the Radarr to different clients.</p>

<h3 id="password">Setting a password in Radarr</h3>
<p>Within the Radarr UI, click the <samp>Settings</samp> icon (the three gears at the top) and then click the <samp>General</samp> tab. Under <samp>Security</samp>, click the dropdown menu next to <samp>Authentication</samp> and select <samp>Forms (Login page)</samp>. Two fields will appear and you'll be able to set a username and password for the web UI. Whilst it's not mandatory you do this, it is a sensible thing to do. You'll need to restart the program for the changes to take effect.</p>

<h3>Configuring clients with Radarr</h3>
<p>In Radarr's user interface click the <samp>Settings</samp> icon (the three gears at the top) and then click <samp>Download Client</samp>. Next, click the <kbd>+</kbd> button. You can choose the torrent client you want to configure from the menu.</p>

<h4 id="rtorrent">rTorrent</h4>
<p>First of all you need to create the rTorrent RPC and this is done by <a href="nginx">switching from apache to nginx</a>.</p>
<dl>
    <dt>Name</dt>
    <dd>Your chosen name for the settings - you can pick what you like</dd>
    <dt>Enable</dt>
    <dd><kbd>Yes</kbd></dd>    
    <dt>Host</dt>
    <dd><kbd><var>server</var>.feralhosting.com</kbd></dd>
    <dt>Port</dt>
    <dd><kbd>443</kbd></dd>
    <dt>Url Path</dt>
    <dd><kbd>/<var>username</var>/rtorrent/rpc</kbd></dd>
    <dt>Use SSL</dt>
    <dd><kbd>Yes</kbd></dd>
    <dt>Username</dt>
    <dd><kbd>rutorrent</kbd> (the word 'rutorrent', <em>not</em> your ruTorrent username)</dd>
    <dt>Password</dt>
    <dd>Your ruTorrent access password</dd>
</dl>
<p>In the above settings, <var>server</var> is replaced by the name of your server (e.g. zeus) and <var>username</var> is your username on the server.</p>

<h4 id="deluge">Deluge</h4>
<p>First make sure <samp>Advanced Settings</samp> have been set to <samp>Shown</samp>, in order for the necessary options to appear.</p>
<dl>
    <dt>Name</dt>
    <dd>Your chosen name for the settings - you can pick what you like</dd>   
    <dt>Enable</dt>
    <dd><kbd>Yes</kbd></dd>    
    <dt>Host</dt>
    <dd><kbd><var>server</var>.feralhosting.com</kbd></dd>
    <dt>Port</dt>
    <dd><kbd>443</kbd></dd>
    <dt>Url Base</dt>
    <dd><kbd>/<var>username</var>/deluge</kbd></dd>
    <dt>Password</dt>
    <dd>Your Deluge web UI password</dd>
    <dt>Use SSL</dt>
    <dd><kbd>Yes</kbd></dd>
</dl>
<p>In the above settings, <var>server</var> is replaced by the name of your server (e.g. zeus) and <var>username</var> is your username on the server.</p>

<h4 id="transmission">Transmission</h4>
<p>First make sure <samp>Advanced Settings</samp> have been set to <samp>Shown</samp>, in order for the necessary options to appear.</p>
<dl>
    <dt>Name</dt>
    <dd>Your chosen name for the settings - you can pick what you like</dd>   
    <dt>Enable</dt>
    <dd><kbd>Yes</kbd></dd>    
    <dt>Host</dt>
    <dd><kbd><var>server</var>.feralhosting.com</kbd></dd>
    <dt>Port</dt>
    <dd><kbd>443</kbd></dd>
    <dt>Url Base</dt>
    <dd><kbd>/<var>username</var>/transmission/</kbd></dd>
    <dt>Username</dt>
    <dd>Your Transmission username</dd>
    <dt>Password</dt>
    <dd>Your Transmission password</dd>
    <dt>Use SSL</dt>
    <dd><kbd>Yes</kbd></dd>
</dl>
<p>In the above settings, <var>server</var> is replaced by the name of your server (e.g. zeus) and <var>username</var> is your username on the server.</p>

<h2 id="uninstallation">Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and its config files - back up important data first!</aside>

<pre><kbd>rm -rf ~/.config/Radarr ~/Radarr; pkill -f Radarr</kbd></pre>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="https://github.com/Radarr/Radarr">Radarr's homepage</a></li>
    <li><a href="https://github.com/Radarr/Radarr/wiki">Radarr wiki</a></li>
</ul>