<h1>rTorrent and ruTorrent</h1>

<p>This article will take you through the installation, usage, and customisation of the torrent client rTorrent and a web UI for it, ruTorrent. For the most part no expertise is required to use the guide. Some areas will require being able to <a href="../slots/ssh">connect to your slot via SSH</a> and you will be instructed to connect via SSH where this is the case.</p>
<p>In this guide <dfn>rTorrent</dfn> refers to the torrent client itself and <dfn>ruTorrent</dfn> to a web UI for it.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#intro">What is ruTorrent?</a></li>
            <li><a href="#installation">Installation</a></li>
            <li>Accessing rTorrent and ruTorrent
                <ul>
                    <li><a href="#access-rutorrent">Accessing ruTorrent</a></li>
                    <li><a href="#access-rtorrent">Accessing rTorrent</a></li>
                </ul>
            </li>
            <li><a href="#start">Starting, stopping and restarting</a></li>
            <li><a href="#configure">Configuring</a></li>
            <li><a href="#usage">Using ruTorrent</a>
                <ul>
                    <li><a href="#basic-considerations">Basic considerations</a></li>
                    <li><a href="#adding-torrents">Adding torrents</a></li>
                    <li><a href="#removing-torrents">Removing torrents</a></li>
                </ul>
            </li>
            <li><a href="#plugins">Plugins</a></li>
            <li><a href="#change-version">Changing version</a></li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="intro">What is ruTorrent?</h2>
<p>ruTorrent is the web UI for a torrent client, rTorrent. It allows you to add, manage and remove torrents to your slot, control rTorrent's settings and do a lot of other things through its plugins.</p>

<h2 id="installation">Installation</h2>
<p>It is very simple to install or reinstall rTorrent and ruTorrent as the Feral web manager supports its installation. Please follow the separate guide to <a href="../getting-started/web-manager#install">installing software from the web manager</a>, selecting ruTorrent from the list (rTorrent will be installed as well).</p>

<h2>Accessing rTorrent and ruTorrent</h2>

<h3 id="access-rutorrent">Accessing ruTorrent</h3>
    <p>To access ruTorrent simply visit the link as provided on your slot's software page. This will be in the format <kbd>https://<var>server</var>.feralhosting.com/<var>user</var>/rutorrent/</kbd>, where <var>server</var> is the name of your server and <var>user</var> is your username.
    </p>

<h3 id="access-rtorrent">Accessing rTorrent</h3>
<p>You will need to connect to your slot via SSH in order to access rTorrent itself. Often problems which seem to spring up in ruTorrent are actually occurring in rTorrent so it is sometimes useful for troubleshooting to access rTorrent.</p>
<p>Once you're logged in to SSH, copy and paste <kbd>screen -r rtorrent</kbd>.</p>

<h2 id="start">Starting, stopping and restarting</h2>
<p>As described in the introduction rTorrent is the torrent client itself whilst ruTorrent is a web UI for it. ruTorrent does not run as a process so this section covers starting, stopping and restarting rTorrent (process name <samp>rtorrent</samp>).</p>
<p>Every five minutes the system will scan the processes running and if rTorrent is not running it will attempt to start it up. You can still control the process manually though, as below:</p>
<dl>
    <dt>start (press <samp>ctrl+a, d</samp> to detach)</dt>
    <dd><kbd>screen -S rtorrent rtorrent<kbd></dd>
    <dt>check running</dt>
    <dd><kbd>pgrep -f /usr/local/bin/rtorrent</kbd></dd>
    <dt>connect to running rtorrent process (press <samp>ctrl+a, d</samp> to detach)</dt>
    <dd><kbd>screen -r rtorrent<kbd></dd>
    <dt>stop</dt>
    <dd><kbd>pkill -f /usr/local/bin/rtorrent</kbd></dd>
    <dt>restart</dt>
    <dd><kbd>pkill -f /usr/local/bin/rtorrent &amp;&amp; sleep 3 &amp;&amp; screen -S rtorrent rtorrent</kbd></dd>
    <dt>kill (force stop)</dt>
    <dd><kbd>pkill -f /usr/local/bin/rtorrent</kbd></dd>
</dl>
<p>If you get any errors running these commands please check the troubleshooting section.</p>
<p>The <samp>check running</samp> command will return a process number if rTorrent is running. If it doesn't return anything, rTorrent is not running.</p>

<h2 id="usage">Using ruTorrent</h2>
<p>This section provides information on the most basic use of ruTorrent.</p>

<h3 id="basic-considerations">Basic considerations</h3>
<p>ruTorrent can handle a large number of torrents but you should bear in mind that you may start to see performance issues with higher numbers of torrents. If you want to use more than around 2,000 you should consider using rTorrent by itself.</p>

<h3 id="adding-torrents">Adding torrents</h3>
<p>To add torrents to ruTorrent click the <samp>Add</samp> button which resembles a globe in the standard theme. It is the left-most icon on ruTorrent's toolbar.</p>
<p>You can also drag and drop the <samp>.torrent</samp> files into ruTorrent from your computer. There is a bug with the https version which means an error will be returned when you do this. If you try to add several <samp>.torrent</samp> files via drag and drop only the first couple may get added. With this in mind you may prefer to either use the http version or add files via one of the other two methods.</p>
<p>Finally, you can also upload the <samp>.torrent</samp> files via (S)FTP, into the directory <samp>~/private/rtorrent/watch</samp>. After around 60 seconds the loaded torrents will be added into ruTorrent.</p>

<h3 id="removing-torrents">Removing torrents</h3>
<p>To remove torrents from ruTorrent, simply select the torrent(s) you want to remove and <samp>right-click</samp> the selection. You can pick <samp>Remove</samp> if you want to remove them from rTorrent without deleting the data from your slot, or <samp>Remove and...</samp> <samp>--></samp> <samp>Delete Data</samp> if you want to remove the data from your slot as well.</p>

<h2 id="plugins">Plugins</h2>
<p>A separate article covers <a href="rutorrent/plugins">installing plugins for ruTorrent</a>.</p>

<h2 id="change-version">Changing version</h2>
<p>A separate article covers <a href="rutorrent/version">changing the version of ruTorrent or rTorrent</a>.</p>

<h2 id="troubleshooting">Troubleshooting</h2>
<dl>
    <dt>I get the error <samp>No connection to rTorrent. Check if it is really running. Check $scgi_port and $scgi_host settings in config.php and scgi_port in rTorrent configuration file</samp></dt>
    <dd>
        <p>This error generally either means that rTorrent has crashed or that ruTorrent has lost communication with it. The fix in either case is to restart rTorrent then refresh the browser running ruTorrent, so please consult the section on starting, stopping and restarting rTorrent.</p>
        <p>If it still does not load even after restarting rTorrent and refreshing ruTorrent, please double check if the disk is full by logging in via SSH and running <kbd>df --si ~/</kbd></p>
    </dd>
    <dt>The ruTorrent loading icon just spins</dt>
    <dd><p>Refresh ruTorrent and your cache by pressing <kbd>ctrl</kbd> + <kbd>F5</kbd> (<kbd>cmd</kbd> + <kbd>F5</kbd> on Mac). If it doesn't help, please try restarting the torrent client as per the section on starting, stopping and restarting rTorrent.</p></dd>
    <dt>When I try to add a torrent nothing happens</dt>
    <dd>
        <p>This could be down to a number of reasons - that the torrent is has a problem with it, that the <samp>.torrent</samp> file is greater than 2MB of that the disk is filled.</p>
        <p>To check the first, upload the <samp>.torrent</samp> file somewhere on your slot, log on to your slot via SSH and run <kbd>transmission-show</kbd> on the torrent: <kbd>transmission-show <var>$torrent_path</var></kbd> where <var>$torrent_path</var> is replaced by the location of the .torrent file.</p>
        <p>If the <samp>.torrent</samp> file itself is greater than 2MB then you can upload it via FTP/SFTP to <samp>~/private/rtorrent/watch</samp></p>
        <p>You can check if your user disk or the server's root filesystem is full by logging in via SSH and running <kbd> df --si / ~/</kbd>. If 100% is displayed under the <samp>Use%</samp> column, please open a ticket.</p>
    </dd>
    <dt>I get the error <samp>rtorrent: Could not lock session directory:</samp></dt>
    <dd><p>This comes about when the previous rTorrent is not closed properly. Simply delete the lock file via SSH with <kbd>rm -f ~/private/rtorrent/work/rtorrent.lock</kbd> and start up rTorrent.</p></dd>
    <dt>When dragging and dropping a torrent, I get an error (or it only adds the first couple)</dt>
    <dd>
        <p>This is caused by the dragging and dropping not playing well with Feral's proxypassed HTTPS ruTorrent URLs. So, there are two "workarounds":</p>
        <p><ol>
            <li>Use another method to upload - the Add function, or uploading to <samp>~/private/rtorrent/watch</samp></li>
            <li>Use the HTTP link (not recommended)</li>
        </ol></p>
    </dd>
    <dt>When I try to access ruTorrent, I just get a lot of 0s down the page</dt>
    <dd><p>Chrome no longer loads sub-resources (e.g., running JavaScript, CSS) when the username and password are applied via the URL. Loading the page directly or using another browser continues to work fine.</p></dd>
</dl>

<h2 id="uninstallation">Uninstalling</h2>
<aside class="alert note">The commands below will completely remove the software, downloaded data and its config files - back up important data first!</aside>
<pre><kbd>kill -9 $(pgrep -fu "$(whoami)" "rtorrent")
rm -rf ~/.rtorrent.rc ~/private/rtorrent</kbd></pre>
<p>These commands act only on your slot, so the software page will not change to reflect the fact that ruTorrent has been removed. The URL and password will remain despite the fact that ruTorrent is gone.</p>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="https://github.com/Novik/ruTorrent">ruTorrent's GitHub page</a></li>
</ul>
