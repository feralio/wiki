<h1>SickChill</h1>
<aside class="alert note">SickChill replaces SickRAGE.</aside>

<p>This article will show you how to install SickChill and configure it to be used with different torrent clients. SickChill is an automatic TV libbrary manager.</p>
<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a>
                <ul>
                    <li><a href="#installation-notes">Installation notes</a></li>
                    <li><a href="#pre-config">Configuring before starting SickChill</a></li>
                </ul>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping and restarting</a>
                <ul>
                    <li><a href="#auto-restart">Automatically restarting SickChill if it is not running</a></li>
                </ul>
            </li>
            <li>Configuring
                <ul>
                    <li><a href="#password">Setting a password in SickChill</a></li>
                    <li><a href="#configure-clients">Configuring clients with SickChill</a>
                        <ul>
                            <li><a href="#rtorrent">rTorrent</a></li>
                            <li><a href="#deluge-web">Deluge (via WebUI)</a></li>
                            <li><a href="#deluged">Deluge (via daemon)</a></li>
                            <li><a href="#transmission">Transmission</a></li>
                            <li><a href="#sabnzbd">SABnzbd</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="installation">Installation</h2>
<p>Install SickChill by logging in via SSH, then copying and pasting the following:</p>
<p><kbd>git clone -b python2.7 https://github.com/SickChill/SickChill.git</kbd></p>

<h3 id="installation-notes">Installation notes</h3>
<p>By default, you will access the software in your browser by using the URL with a port number. If you consider the URL without a port number to be neater, you will need to <a href="../slots/proxypass">configure a proxypass</a>.</p>
<p>SickChill may need an updated version of Unrar if you need that facility in post-processing. Please see this guide to <a href="unrar">upgrading Unrar</a>.</p>

<h3 id="pre-config">Configuring before starting SickChill</h3>
<aside class="alert note">The commands below assume you have <a href="pip">installed a version of Pip in a virtualenv</a> to be able to painlessly upgrade and install modules</aside>
<p>Once you've got a copy of Pip running in a virtual environment, copy and paste the following to set up various components of SickChill:</p>
<pre><kbd>~/pip/bin/pip install --upgrade -r ~/SickChill/requirements.txt
~/pip/bin/pip install --upgrade pyopenssl</kbd></pre>

<p>A config.ini file needs to be created and tweaked slightly before we can properly run SickChill. You can copy and paste the commands below to do this.</p>
<pre><kbd>~/pip/bin/python ~/SickChill/SickBeard.py -d --pidfile="$HOME/SickChill/SickChill.pid"
pkill -9 -fu "$(whoami)" 'SickChill/SickBeard.py'
rm -rf ~/SickChill/SickChill.pid &amp;&amp; sleep 10
mkdir ~/.SickChill.tv.shows
sed -ri 's|web_port = (.*)|web_port = '"$(shuf -i 10001-32001 -n 1)"'|g' ~/SickChill/config.ini
sed -ri 's|web_root = (.*)|web_root = /'"$(whoami)"'/sickchill|g' ~/SickChill/config.ini
sed -ri 's|launch_browser = 1|launch_browser = 0|g' ~/SickChill/config.ini
sed -ri 's#root_dirs = (.*)#root_dirs = '"$HOME"'/.SickChill.tv.shows#g' ~/SickChill/config.ini</kbd></pre>

<h2 id="start-stop-restart">Starting, stopping and restarting</h2>
<p>This section covers the SickChill process - starting it, stopping it and restarting it. It also covers checking if the process is running, in case that becomes necessary.</p>
<p>You can start SickChill and print its URL with the following:</p>
<pre><kbd>~/pip/bin/python ~/SickChill/SickBeard.py -d --pidfile="$HOME/SickChill/SickChill.pid" &amp;&amp; echo http://$(hostname -f):$(sed -rn 's/(.*)web_port = //p' ~/SickChill/config.ini)/$(whoami)/sickchill</kbd></pre>
<p>If nothing happens or there is an error it could be that the port that was picked in the configuration step in use. Simply run these commands again to try with a different number (you'll need to edit the proxypass if you have one set up):</p>
<pre><kbd>sed -ri 's|web_port = (.*)|web_port = '"$(shuf -i 10001-32001 -n 1)"'|g' ~/SickChill/config.ini
~/pip/bin/python ~/SickChill/SickBeard.py -d --pidfile="$HOME/SickChill/SickChill.pid"</kbd></pre>

<p>If you need to check that it's running, you can run the command below. If the process is running a list of relevant process ID numbers will be listed. If nothing is listed, the process is not running.</p>
<p><kbd>pgrep -fu "$(whoami)" "python $HOME/SickChill/SickBeard.py -d"</kbd></p>

<p>The SickChill process is stopped by executing:</p>
<p><kbd>pkill -fu "$(whoami)" 'SickChill/SickBeard.py'</kbd></p>

<p>If SickChill needs to be restarted, copy and paste this command:</p>
<pre><kbd>pkill -fu "$(whoami)" 'SickChill/SickBeard.py' &amp;&amp; rm -rf ~/SickChill/SickChill.pid &amp;&amp; ~/pip/bin/python ~/SickChill/SickBeard.py -d --pidfile="$HOME/SickChill/SickChill.pid"</kbd></pre>

<p>Please note that if you want to stop the process for SickChill, it's best to check it afterwards to make sure it has stopped. If the process has crashed and will not stop when requested, you can kill it with:</p>
<p><kbd>pkill -9 -fu "$(whoami)" 'SickChill/SickBeard.py'</kbd></p>

<h3 id="auto-restart">Automatically restarting SickChill if it is not running</h3>
<p>Cron jobs can be used to check if SickChill is running and start it up if it is not. There is a separate page on <a href="../slots/cron">configuring cron jobs</a>.</p>

<h2>Configuring</h2>
<p>This section covers setting a password in SickChill and connecting the SickChill to different clients.</p>

<h3 id="password">Setting a password in SickChill</h3>
<p>Within the SickChill UI, click the <kbd>Config</kbd> icon (the two gears at the top) and then click <kbd>General</kbd>. Next, click the <kbd>Interface</kbd> tab. Near the bottom of the fields you'll be able to set a username and password for the web UI. Whilst it's not mandatory you do this, it is a sensible thing to do.</p>

<h3 id="configure-clients">Configuring clients with SickChill</h3>
<p>In SickChill's user interface click the <kbd>Config</kbd> icon (the two gears at the top) and then click <kbd>Search Settings</kbd>. Next, click the <kbd>Torrent Search</kbd> tab then check/tick the <samp>enable torrent search providers</samp> box. You can choose the torrent client you want to configure from the menu.</p>

<h4 id="rtorrent">rTorrent</h4>
<p>First of all you need to create the rTorrent RPC and this is done by <a href="nginx">switching from apache to nginx</a>.</p>

<dl>
    <dt>Torrent host:port</dt>
    <dd><kbd>https://<var>server</var>.feralhosting.com/<var>user</var>/rtorrent/rpc/<kbd> (replace <var>server</var> with your server name and <var>user</var> with your username)</dd>
    <dt>Http Authentication</dt>
    <dd><kbd>Basic</kbd></dd>    
    <dt>Client username</dt>
    <dd><kbd>rutorrent</kbd> (the word 'rutorrent', <em>not</em> your ruTorrent username)</dd>
    <dt>Client password</dt>
    <dd>The same password as you use to access ruTorrent</dd>
</dl>

<h4 id="deluge-web">Deluge (via webUI)</h4>
<dl>
    <dt>Torrent host:port</dt>
    <dd>https://<var>server</var>.feralhosting.com/<var>user</var>/deluge/</dd>
    <dd>(replace <var>server</var> with your server name and <var>user</var> with your username)</dd>
    <dt>Client password</dt>
    <dd>Your Deluge webUI password</dd>    
    <dt>Downloaded files location</dt>
    <dd>Make sure it's set to a location you're happy with (or make sure it's blank to use Deluge's default).</dd>
</dl>

<h4 id="deluged">Deluge (via daemon)</h4>
<p>There are a couple of commands we need to run via SSH first to obtain the daemon port and password (the password is not the same as the webUI password).</p>
<p>To find the port run this command:</p>
<p><kbd>sed -rn 's/(.*)"daemon_port": (.*),/\2/p' ~/.config/deluge/core.conf</kbd></p>
<p>To find the password run this command:</p>
<p><kbd>sed -rn "s/$(whoami):(.*):(.*)/\1/p" ~/.config/deluge/auth</kbd></p>
<p>Armed with the port and password, enter the details as follows:</p>
<dl>
    <dt>Torrent host:port</dt>
    <dd>https://<var>server</var>.feralhosting.com:<var>port</var> (replace <var>server</var> with your server name and <var>port</var> with the results of the command above)</dd>
    <dt>Client username</dt>
    <dd>Your Deluge daemon username (unless you've changed it, it'll be your Feral username)</dd>    
    <dt>Client password</dt>
    <dd>Your Deluge daemon password</dd>    
    <dt>Downloaded files location</dt>
    <dd>Make sure it's set to a location you're happy with (or make sure it's blank to use Deluge's default).</dd>
</dl>

<h4 id="transmission">Transmission</h4>
<dl>
    <dt>Torrent host:port</dt>
    <dd>https://<var>server</var>.feralhosting.com/<var>user</var> (replace <var>server</var> with your server name and <var>user</var> with your username)</dd>
    <dt>Transmission RPC URL</dt>
    <dd><kbd>transmission/rpc</kbd></dd>    
    <dt>Client username</dt>
    <dd>The username you use to access Transmission</dd>    
    <dt>Client password</dt>
    <dd>The same password as you use to access Transmission</dd>
</dl>

<h4 id="sabnzbd">SABnzbd</h4>
<p>SickChill can also search for NZB files. This section covers connecting SickChill to <a href="sabnzbd">SABnzbd</a>.</p>
<dl>
    <dt>SABnzbd server URL</dt>
    <dd>https://<var>server</var>.feralhosting.com:<var>port</var>/sabnzbd/ (replace <var>server</var> with your server name and <var>port</var> with your HTTPS port for SABnzbd)</dd>    
    <dt>SABnzbd username</dt>
    <dd>The username you set for the SABnzbd UI</dd>    
    <dt>SABnzbd password</dt>
    <dd>The password you set for the SABnzbd UI</dd>    
    <dt>SABnzbd API key</dt>
    <dd>Find this in the <samp>General</samp> section of SABnzbd's settings</dd>
</dl>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>If you experience issues or crashes using SickChill the first thing to try and do is restart the software using the command listed in the section above, <i>Starting, stopping and restarting</i>.</p>
<dl>
    <dt>When trying to install the requirements, I get <samp>bash: <var>$HOME</var>/pip/bin/pip: No such file or directory</samp></dt>
    <dd><p>Make sure you've <a href="pip">installed your own version of Pip</a></p></dd>
    <dt>When trying to run SickChill, I get <samp>Failed to import required libs...</samp></dt>
    <dd><p>Please make sure you've a) installed a version of Pip to your slot (see above) and b) are calling Python using <samp>~/pip/bin/python</samp> instead of just <samp>python</samp></p></dd>
    <dt>When I click <kbd>Test Connection</kbd> I get the error <samp>Error: Unable to connect to rTorrent'</samp></dt>
    <dd><p>If rTorrent is running and you've made the <a href="nginx">switch to nginx</a> this error likely means some of the details you've entered are not correct. Please double-check that your username is rutorrent (as in the actual word, "rutorrent", not the username you use to log in to ruTorrent). If you changed your ruTorrent password after switching to nginx, you'll need to use the old password. If you no longer recall it, please reinstall nginx.</p></dd>
    <dt>I went to start up SickChill and received an error <samp>'...SickChill.pid already exists. Exiting.'</samp></dt>
    <dd>
        <p>Assuming you're using the default locations please run the following command before trying the start up command again:</p>
        <p><kbd>rm ~/SickChill/SickChill.pid</kbd></p>
    </dd>
    <dt>When I use pip I get an error <samp>IndexError: list index out of range</samp></dt>
    <dd><p>Please try using the option <kbd>--no-use-wheel</kbd> when using pip.</p></dd>
</dl>

<h2 id="uninstallation">Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and its config files - back up important data first!</aside>
<pre><kbd>pkill -9 -fu "$(whoami)" 'SickChill/SickBeard.py'
rm -rf ~/SickChill ~/.SickChill.tv.shows</kbd></pre>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="https://SickChill.github.io/">SickChill's homepage</a></li>
</ul>