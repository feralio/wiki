#!/usr/bin/env bash
user=$(whoami)

function port() {
  LOW_BOUND=$1
  UPPER_BOUND=$2
  comm -23 <(seq ${LOW_BOUND} ${UPPER_BOUND} | sort) <(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) | shuf | head -n 1
}

function _download() {
  latest=$(curl -sL https://api.github.com/repos/autobrr/autobrr/releases/latest | grep "linux_x86_64" | grep browser_download_url | grep ".tar.gz" | cut -d \" -f4) || {
    echo "Failed to query GitHub for latest version"
    exit 1
  }

  if ! curl "$latest" -L -o "$HOME/autobrr.tar.gz"; then
    echo "Download failed, exiting"
    exit 1
  fi

  echo "Archive downloaded"

  echo "Extracting archive"
  mkdir -p "$HOME/bin/"

  tar xfv "$HOME/autobrr.tar.gz" --directory "$HOME/bin/" || {
    echo "Failed to extract"
    exit 1
  }

  echo "Archive extracted"

  rm -f "$HOME/autobrr.tar.gz"
}

# Create nginx conf. Takes port as argument $1
function _nginx_conf() {
  cat <<EOF | tee "${HOME}/.nginx/conf.d/000-default-server.d/autobrr.conf" >/dev/null
location /autobrr {
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$http_x_host;
    proxy_set_header X-NginX-Proxy true;

    rewrite /autobrr/(.*) /\$1 break;
    proxy_pass http://10.0.0.1:$1/;
    proxy_redirect off;
}
EOF

  # reload nginx
  /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf

  echo "Nginx conf created"
}

# Remove nginx conf
function _nginx_remove() {
  if [ -f "${HOME}/.nginx/conf.d/000-default-server.d/autobrr.conf" ]; then
    rm "${HOME}/.nginx/conf.d/000-default-server.d/autobrr.conf"

    # reload nginx
    /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf

    echo "Nginx conf removed"
  fi
}

function _install() {
  _download
  port=$(port 10000 12000)
  sessionSecret="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c16)"

  mkdir -p "$HOME/.config/autobrr/"
  mkdir -p "$HOME/.config/autobrr/logs"

  cat >"$HOME/.config/autobrr/config.toml" <<CFG
# config.toml
# Hostname / IP
#
# Default: "localhost"
#
host = "0.0.0.0"
# Port
#
# Default: 7474
#
port = ${port}
# Base url
# Set custom baseUrl eg /autobrr/ to serve in subdirectory.
# Not needed for subdomain, or by accessing with the :port directly.
#
# Optional
#
baseUrl = "/$user/autobrr/"
#
# autobrr logs file
# If not defined, logs to stdout
#
# Optional
#
logPath = "$HOME/.config/autobrr/logs/autobrr.log"
# Log level
#
# Default: "DEBUG"
#
# Options: "ERROR", "DEBUG", "INFO", "WARN", "TRACE"
#
logLevel = "DEBUG"
# Session secret
#
sessionSecret = "${sessionSecret}"
CFG

  read -rep "Please set a password for your autobrr user ${user}> " password
  echo "${password}" | "$HOME/bin/autobrrctl" --config "$HOME/.config/autobrr" create-user "$user" || {
    echo "Failed to execute autobrrctl command"
    exit 1
  }

  # Create nginx conf
  _nginx_conf "$port"

  cat >"$HOME/.autobrr.cron" <<CRONC
#!/bin/bash
if pgrep -f "$HOME/bin/autobrr --config=$HOME/.config/autobrr" > /dev/null
then
    echo "autobrr is running."
else
    echo "autobrr is not running, starting autobrr"
    screen -dmS autobrr /bin/bash -c "$HOME/bin/autobrr --config=$HOME/.config/autobrr"
fi
exit
CRONC
  chmod +x "$HOME/.autobrr.cron"

  screen -dmS autobrr /bin/bash -c "$HOME/bin/autobrr --config=$HOME/.config/autobrr"

  echo ""
  echo "Adding to crontab"
  (
    crontab -l -u $(whoami)
    echo -e "*/5 * * * * $HOME/.autobrr.cron >/dev/null 2>&1"
  ) | crontab -u $(whoami) -

  echo ""
  echo "Installation complete!"
  echo ""
  echo "Now browse to your autobrr instance at https://$HOSTNAME.feralhosting.com/$user/autobrr/"
}

function _update() {
  if [[ ! -f $HOME/bin/autobrr ]]; then
    echo "Autobrr not installed!"
    exit 1
  fi
  # if this takes longer than 5 mins this is sad.
  pkill "autobrr" || exit 1

  # backup
  _backup_install

  rm -f "$HOME/bin/autobrr*"
  _download

  [[ $(pgrep -f 'autobrr') ]] || screen -dmS autobrr /bin/bash -c "$HOME/bin/autobrr --config=$HOME/.config/autobrr"
  echo "autobrr has been updated!"
}

function _remove() {
  if [[ ! -f $HOME/bin/autobrr ]]; then
    echo "Autobrr not installed!"
    exit 1
  fi
  echo "Killing autobrr..."
  pkill "autobrr"

  # backup
  _backup_install

  _nginx_remove

  crontab -l | sed -e "/autobrr/d" | crontab -u $(whoami) -
  echo "Removed crontab entry"

  rm -rf "$HOME/.config/autobrr"
  rm -f "$HOME/bin/autobrr"
  rm -f "$HOME/bin/autobrrctl"
  rm -f "$HOME/.autobrr.cron"

  echo "autobrr has been removed"
}

# Backup install
function _backup_install() {
  if [ ! -d "${HOME}/backup" ]; then
    mkdir -p "${HOME}/backup"
  fi

  if [ -d "${HOME}/.config/autobrr" ]; then
    backup="${HOME}/backup/autobrr-$(date +"%FT%H%M").bak.tar.gz"
    echo
    echo "Creating a backup of the current instance's AppData directory.."
    tar -czf "${backup}" -C "${HOME}/.config" "autobrr"
    echo
    echo "Created backup of autobrr: $backup"
  fi
}

# if autobrr already exists, then ask what to do
if [ ! -d "${HOME}/.config/autobrr" ]; then
  echo
  echo "Installing autobrr..."
  _install
  exit 0
else
  echo
  echo "Existing install of autobrr detected. Install will be backed up."

  select status in "Install" "Update" "Uninstall" "Backup" quit; do

    case ${status} in
    "Install")
      status="install"
      _install
      exit 0
      ;;
    "Update")
      status="update"
      _update
      exit 0
      ;;
    "Uninstall")
      status="uninstall"
      _remove
      exit 0
      ;;
    "Backup")
      status="backup"
      _backup_install
      exit 0
      ;;
    quit)
      exit 0
      ;;
    *)
      echo "Invalid option $REPLY"
      exit 0
      ;;
    esac
  done
fi

exit 0
