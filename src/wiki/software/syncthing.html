<h1>SyncThing</h1>
<p>This article will show you how to install and configure SyncThing. This software will allow you to sync to another computer or device via the BitTorrent protocol.</p>
<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a>
                <ul>
                    <li><a href="#pre-config">Configuring before starting SyncThing</a></li>
                </ul>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping and restarting</a>
                <ul>
                    <li><a href="#auto-restart">Automatically restarting SyncThing if it is not running</a></li>
                </ul>
            </li>
            <li>Configuring
                <ul>
                    <li><a href="#password">Creating a username and password for the webUI</a></li>
                    <li><a href="#concurrency">Limiting concurrency</a></li>
                </ul>
            </li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details>

<h2 id="installation">Installation</h2>
<p>Install SyncThing by logging in via SSH, then copying and pasting the following:</p>
<pre><kbd>wget -qO ~/syncthing.tar.gz https://github.com/syncthing/syncthing/releases/download/v1.9.0/syncthing-linux-amd64-v1.9.0.tar.gz
mkdir -p ~/bin; tar xvzf ~/syncthing.tar.gz -C ~/; mv ~/syncthing-linux-amd64-v1.9.0/syncthing ~/bin/; chmod +x ~/bin/syncthing; ~/bin/syncthing -generate="~/.config/syncthing"
rm ~/syncthing.tar.gz ; rm -rf ~/syncthing-linux-amd64-v1.9.0</kbd></pre>

<h3 id="pre-config">Configuring before starting Syncthing</h3>
<p>The config needs to be tweaked before you can run Syncthing. Copy and paste these commands:</p>
<pre><kbd>sed -i 's|127.0.0.1:[0-9]*|0.0.0.0:'$(shuf -i 10001-49000 -n 1)'|g' ~/.config/syncthing/config.xml
sed -i 's|&lt;localAnnounceEnabled&gt;true&lt;/localAnnounceEnabled&gt;|&lt;localAnnounceEnabled&gt;false&lt;/localAnnounceEnabled&gt;|g' ~/.config/syncthing/config.xml
sed -i 's|&lt;natEnabled&gt;true&lt;/natEnabled&gt;|&lt;natEnabled&gt;false&lt;/natEnabled&gt;|g' ~/.config/syncthing/config.xml</kbd></pre>
<p>Once started you must <a href="#password">set a password</a> and <a href="#concurrency">limit concurrency</a>.</p>

<h2 id="start-stop-restart">Starting, stopping and restarting</h2>
<p>This section covers the SyncThing process - starting it, stopping it and restarting it. It also covers checking if the process is running, in case that becomes necessary.</p>
<dl>
    <dt>start</dt>
    <dd><kbd>screen -dmS syncthing ~/bin/syncthing &amp;&amp; echo http://$(hostname -f):$(sed -rn 's|.*&lt;address&gt;0.0.0.0:(.*)&lt;/address&gt;.*|\1|p' ~/.config/syncthing/config.xml)<kbd></dd>
    <dt>check running</dt>
    <dd><kbd>pgrep -laf syncthing</kbd></dd>
    <dt>stop</dt>
    <dd><kbd>pkill -f syncthing</kbd></dd>
    <dt>restart</dt>
    <dd><kbd>pkill -f syncthing &amp;&amp; sleep 5 &amp;&amp; screen -dmS syncthing ~/bin/syncthing &amp;&amp; echo http://$(hostname -f):$(sed -rn 's|.*&lt;address&gt;.0.0.0:(.*)&lt;/address&gt;.*|\1|p' ~/.config/syncthing/config.xml)</kbd></dd>
    <dt>kill (force stop)</dt>
    <dd><kbd>pkill -9 -f syncthing</kbd></dd>
</dl>
<p>The <samp>check running</samp> command will return a process number if SyncThing is running. If it doesn't return anything, SyncThing is not running.</p>

<h3 id="auto-restart">Automatically restarting SyncThing if it is not running</h3>
<p>Cron jobs can be used to check if SyncThing is running and start it up if it is not. There is a separate page on <a href="../slots/cron">configuring cron jobs</a>.</p>

<h2>Configuring</h2>

<h3 id="password">Creating a username and password for the webUI</h3>
<p>Once you access the webUI using the URL provided by the start command you'll get a popup asking whether you want to send anonymouse usage reporting, dismiss it by accepting or not. Once it's done you will see an information that there is no password set up for webUI, click on Settings button and proceed to GUI tab to set password for the account.</p>

<h3 id="concurrency">Limiting concurrency</h3>
<p>By default syncthing will use as many threads as there are cores on the server. This concurrency leads to the disk being easily overwhelmed slowing down performance for your own programs and others on the server. Here's how to limit concurrency so that it doesn't cause any problems:</p>
<p>At the <a href="https://docs.syncthing.net/users/advanced.html">top-right of the menu</a>, select the drop-down "Actions" then "Advanced". Under "Options" change "Max Folder Concurrency" to 0 (all cores) to 1. Then for each "Folder ..." listed change "Hashers" and "Copiers" from 0 to 1. This will then also need to be applied to the "Default Folder" to auto-apply against any newly created folders. You will then need to click "Save" to apply the settings.</p>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>If you experience issues or crashes using SyncThing the first thing to try and do is restart the software using the command listed in the section above, Starting, stopping and restarting.</p>

<h2 id="uninstallation">Uninstallation</h2>
<p>The commands below will completely remove the software and its config files - back up important data first!</p>
<pre><kbd>pkill -9 -f syncthing
rm -rf ~/.config/syncthing/ ~/bin/syncthing ~/Sync</kbd></pre>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="https://syncthing.net">SyncThing's homepage</a>
        <ul>
            <li><a href="https://docs.syncthing.net/">Support documentation</a></li>
            <li><a href="https://forum.syncthing.net/">Community</a></li>
        </ul>
    </li>
</ul>