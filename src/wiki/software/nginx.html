<h1>nginx</h1>
<p>Though nginx is now the default web server software, you may still be using (or may have switched to) Apache. Nginx can be used to configure and run pages and software accessible via HTTP and HTTPS. You'll likely find nginx easier to configure, faster and more lightweight than Apache.</p>

<p>You'll need to execute some commands via SSH to get the full use out of this software, though it can be set up via FTP. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a>
                <ul>
                    <li><a href="#installation-notes">Installation notes</a></li>
                    <li><a href="#pre-config">Configuring before starting nginx</a></li>
                </ul>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping and restarting</a></li>
            <li><a href="#usage">Using nginx</a></li>
            <li><a href="#advanced">Advanced usage</a>
                <ul>
                    <li><a href="#https">HTTPS</a></li>
                    <li><a href="#force-https">Force HTTPS</a></li>
                    <li><a href="#php">PHP version</a></li>
                </ul>
            </li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details>

<h2 id="installation">Installation</h2>
<p>Install nginx by logging in via SSH, then copying and pasting the following:</p>
<p><kbd>mkdir ~/.nginx</kbd></p>
<p>You can also create this directory using your (S)FTP software of choice. Either way, every 5 minutes the system will scan slots and if it sees an empty .nginx directory it will stop Apache and install nginx for you. That's it!</p>

<h3 id="installation-notes">Installation notes</h3>
<p>In addition to the basic installation, the following will also automatically happen (or be attempted):</p>
<ul>
    <li>PHP will be configured</li>
    <li>Domains served from <samp>~/www</samp></li>
    <li>ruTorrent will be password protected using a <samp>.conf</samp>> file</li>
    <li>An SCGI mount point will be created for rTorrent at /<var>username</var>/RPC</li>
    <li>Access to areas of ruTorrent which don't need web access will be denied</li>
    <li>Access to all files beginning with <samp>.ht</samp> will be denied</li>
    <li>Access to any directory with a <samp>.htaccess</samp> file will be denied</li>
</ul>

<h3 id="pre-config">Configuring before starting nginx</h3>
<p>As above, the auto-configuration will seek to block access to anything reliant on <samp>.htaccess</samp> files. Nginx relies on its own configuration files instead - you should look to either <a href="http://winginx.com/htaccess">convert .htaccess to an nginx config file</a> or, if the software page has a section for nginx users reconfigure according to those instructions. You can review by comparison, using the documentation for the various <a href="http://httpd.apache.org/docs/2.2/index.html">Apache</a> and <a href="http://wiki.nginx.org/Main">nginx</a> documentation.</p>
<p>Once you've reviewed and amended the previous Apache configurations for nginx you can remove its <samp>deny-htaccess.conf</samp> file from <samp>~/.nginx/conf.d/000-default-server.d</samp></p>

<h2 id="start-stop-restart">Starting, stopping and restarting</h2>
<p>As nginx is started by the system, you'll only be able to kill it: the system will start it up for you automatically every five minutes.</p>
<p>To kill nginx simply run this command via SSH:
<p><kbd>pkill -fu "$(whoami)" 'nginx'</kbd></p>
<p>You can then check if any part of nginx is still running with <kbd>ps x | grep nginx | grep -v grep</kbd>.</p>

<h2 id="usage">Using nginx</h2>
<p>Nginx is used via config files which tell the web server what you want to do. This page won't go into much detail on that - software in our <a href="https://www.feralhosting.com/wiki#software">software section</a> will contain the specific information you need to configure for nginx.</p>
<p>After you've made your changes you'll need to reload nginx. This is not the same as restarting it (though restarting it would of course reload the configs too). You can reload by running this command:</p>
<p><kbd>/usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf</kbd></p>
<p>At this point you'll receive an alert. This is normal and is not an error as such:</p>
<pre><samp>[server ~] /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf
nginx: [alert] could not open error log file: open() "/var/log/nginx/error.log" failed (13: Permission denied)
2017/06/03 07:49:30 [notice] 7309#0: signal process started
</samp></pre>
<p>Errors generally stop nginx from coming back to life post-reload, so you can always run the check command from above to confirm if nginx is running.</p>

<h2 id="advanced">Advanced usage</h2>
<h3 id="https">HTTPS</h3>
<p>Every slot comes with HTTP and HTTPS set up at: <samp>http://<var>username</var>.<var>server</var>.feralhosting.com/</samp> and <samp>https://<var>server</var>.feralhosting.com/<var>username/</var></samp> (you will need to replace your username and server).</p>
<p>For non-Feral domains HTTPS can be set up if you supply your own certificates. In the past this required using and setting up a forwarded port; this is no longer the case. You will need to create two files and then wait five minutes for them to be installed:</p>
<dl>
    <dt>~/www/<var>example.com</var>/https.crt</dt>
    <dd>A PEM certificate for use in nginx's <a href="http://nginx.org/en/docs/http/ngx_http_ssl_module.html#ssl_certificate">ssl_certificate</a>. The format of a PEM <samp>.crt</samp> file contains one or more certificates each looking like a much longer version of:
<pre><samp>-----BEGIN CERTIFICATE-----
<var>random text</var>
-----END CERTIFICATE-----
</samp></pre></dd>
    <dt>~/www/<var>example.com</var>/https.key</dt>
    <dd>An RSA certificate for use in nginx's <a href="http://nginx.org/en/docs/http/ngx_http_ssl_module.html#ssl_certificate_key">ssl_certificate_key</a>. The format of the <samp>.key</samp> is a single certificate and looks like a much longer version of:
<pre><samp>-----BEGIN RSA PRIVATE KEY-----
<var>random text</var>
-----END RSA PRIVATE KEY-----
</samp></pre></dd>
</dl>
<p>A source for free certificates can be found at <a href="https://www.sslforfree.com/">www.sslforfree.com</a>.</p>

<h3 id="force-https">Force HTTPS</h3>
<p>The nginx variable <samp>$http_x_forwarded_proto</samp> will be either <samp>http</samp> or <samp>https</samp>. Add the following to each <samp>server { ... }</samp> stanza under <samp>~/.nginx/conf.d</samp> to force a redirect to HTTPS:</p>
<pre><samp>if ( $http_x_forwarded_proto != 'https' ) {
    return 301 https://$host$request_uri;
}</samp></pre>

<h3 id="php">PHP version</h3>
<p>Your slot will automatically execute any file ending in <samp>.php</samp> as a PHP file. Feral uses the PHP version installed by the OS, relies on it for security updates and tests software against this version. If you require a specific version of PHP then you can install your own version to your home folder via:</p>
<ol>
    <li>Select the latest PHP version from <a href="https://www.php.net/downloads">php.net/downloads</a>.</li>
    <li>Follow the generic install guide to <a href="https://www.feralhosting.com/wiki/slots/generic-install-guide">install it to your home folder</a> from source running this alternative configure step: <samp>./configure --enable-fpm --prefix=$HOME</samp></li>
    <li>Create a symlink in <samp>~/.nginx/php/start</samp> to point to your installed <samp>php-fpm</samp> executable. Try <kbd>ln -s ~/sbin/php-fpm ~/.nginx/php/start</kbd> to create the symlink.</li>
    <li>Restart PHP with the command <samp>pkill php-fpm</samp> and wait five minutes for it to be restarted.</li>
    <li>Periodically update PHP by installing (steps 1 and 2). Updates are crucial to keeping your slot secure.</li>
</ol>
<p>This is considered an advanced usage of a slot because it requires a lot of up-front Linux knowledge and ongoing maintenance. It should only be done if you have a specific reason.</p>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>If you experience issues or crashes using nginx the first thing to try and do is restart the software using the command listed in the section above, "Starting, stopping and restarting"</p>
<dl>
    <dt>Nginx will not start</dt>
    <dd>
        <p>This can happen if nginx is reloaded or restarted via any means and is normally related to a config change you've made that is invalid. Even if you made the changes a long time ago, since nginx will only try to load them on reload/restart it might be a while before the error comes to light. You can often see the problematic change by running the reload command:</p>
        <pre><kbd>[server ~/.nginx/conf.d/000-default-server.d] /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf
nginx: [alert] could not open error log file: open() "/var/log/nginx/error.log" failed (13: Permission denied)
2017/06/03 07:50:11 [emerg] 15787#0: invalid port in upstream "10.0.0.1:/" in /media/sda1/user/.nginx/conf.d/000-default-server.d/sonarr.conf:8</kbd></pre>
        <p>In the above example, there isn't a port in the Sonarr config, so naturally an invalid port issue arises. In this case, adding the correct port Sonarr is listening is what's required. Alternatively, if no longer required, you can delete the config. Either way, you'll need to run the reload command again.</p>
    </dd>
</dl>

<h2 id="uninstallation">Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and config files - back up important data first!</aside>
<pre><code>pkill -9 -fu "$(whoami)" 'nginx'
rm -rf ~/.nginx</code></pre>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="http://nginx.org/">nginx homepage</a>
        <ul>
            <li><a href="http://nginx.org/en/docs/beginners_guide.html">Beginner's guide</a></li>
            <li><a href="http://nginx.org/en/docs/">Document section</a></li>
        </ul>
    </li>
</ul>
